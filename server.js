var express = require('express');
var bp = require('body-parser');
var app = express();

app.use(express.static(__dirname));
app.use(bp.json());
app.use(bp.urlencoded({extended:false}))

var messages = [
    {name:"Tamara" , message:"Ortografia"},
    {name:"Natalia" , message: "Culie bn"}
];

app.get('/messages', (req , res) => res.send(messages));

app.post('/messages', (req , res) => {
    messages.push(req.body);
    res.sendStatus(200);
});

app.listen(3000, ()=> {
    console.log("server en escucha");
});
